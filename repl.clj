(require '[clojure.spec.alpha :as s])
(require '[clojure.string :as string])
(require '[clojure.test.check.generators :as gen])

(s/def ::infix (s/or :int pos-int? :operator #{\+ \- \* \/}))
(gen/sample (s/gen ::infix))

(defn run[exp]
  "eval the infix math"
  (if (= 3 (count exp))
    (let [txt (string/join " " exp)]
      (eval (read-string (str "(" txt ")"))))
    (if (int? (second exp))
      ;right-group
      ;first parameter is integer, recursive right hand side
      (do #_(println "left")
          (run (flatten (list (take 2 exp) (run (drop 2 exp))))))
      ;left-group
      (do 
        #_(println (rest (drop-last exp)))
        #_(println (last exp))
        #_(println (flatten (list (first exp) (run (rest (drop-last exp))) (last exp))))
        (run (flatten (list (first exp) (run (rest (drop-last exp))) (last exp)))))
      )))

(comment

  (run '(+ 2 3))

  (run '(+ 1 + 2 3))

  (run '(+ + 1 2 3))

  (run '(+ 1 + 1 + 2 3))

  )

(doseq [n (range 100)]
  ; 3 items to stat with
  (let [exp (gen/sample (s/gen ::infix) 7)]
    (try
      (println exp "returns" (run exp))
      (catch Exception e (str "caught exception: " (.getMessage e)))
      )
    ))
